module Main(main) where
import Control.Monad.Eff
import Control.Monad.Eff.Console
import Control.Monad.Eff.WebGL
import Control.Monad.ST
import Graphics.WebGL
import Graphics.WebGLAll
import Data.Matrix4 as M
import Data.Matrix (toArray) as M
import Prelude
import Math (sin)
import Data.Ord (abs)
import State
import World
import Data.Vector2 as V
import Data.ArrayBuffer.Types as T

type MyBindings =
  ( aVertexPosition :: Attribute Vec2
  , aVertexColor :: Attribute Vec4
  , uProjectionMatrix :: Uniform Mat4
  , uModelViewMatrix:: Uniform Mat4 )

shaders :: Shaders (Record MyBindings)
shaders = Shaders
  """
  precision highp float;
  varying vec4 vVertexColor;
  void main() {
    gl_FragColor = vVertexColor;
  }
  """
  """
  attribute vec2 aVertexPosition;
  attribute vec4 aVertexColor;
  uniform mat4 uProjectionMatrix;
  uniform mat4 uModelViewMatrix;
  varying vec4 vVertexColor;
  void main() {
    gl_PointSize = 4.0;
    gl_Position = uProjectionMatrix * uModelViewMatrix * vec4(aVertexPosition, 0.0, 1.0);
    vVertexColor = aVertexColor;
  }
  """

loop draw =
  let
    next _ = do
      requestAnimationFrame (next unit)
      draw
  in
    requestAnimationFrame (next unit)

projection = M.makeOrtho (-1.0) 1.0 (-1.0) 1.0 (-1.0) 1.0
modelView = M.identity

setupView bindings = do
  setUniformFloats bindings.uProjectionMatrix (M.toArray projection)
  setUniformFloats bindings.uModelViewMatrix (M.toArray modelView)

type Buf =
  { colors :: Buffer T.Float32
  , positions :: Buffer T.Float32 }

initBuf colors positions = do
  c <- makeBufferFloat colors
  p <- makeBufferFloat positions
  pure { colors: c, positions: p }

fillBuf buf colors positions = do
  fillBuffer buf.colors 0 colors
  fillBuffer buf.positions 0 positions

drawBuf mode bindings buf = do
  bindBufAndSetVertexAttr buf.colors bindings.aVertexColor
  drawArr mode buf.positions bindings.aVertexPosition

mainWebGLWithShaders context bindings = do
  clearColor 0.0 0.0 0.0 1.0
  ref <- stateInit
  setupView bindings
  state <- stateGet ref
  buf <- initBuf (colors state.particles) (positions state.particles)
  lbuf <- initBuf (getSprings color state) (getSprings pos state)
  loop $ do 
    state <- stateNext ref
    clear [COLOR_BUFFER_BIT]
    fillBuf buf (colors state.particles) (positions state.particles)
    fillBuf lbuf (getSprings color state) (getSprings pos state)
    drawBuf POINTS bindings buf
    drawBuf LINES bindings lbuf

mainWebGL context = withShaders shaders logShow (mainWebGLWithShaders context)

main = runWebGL "glcanvas" logShow mainWebGL
