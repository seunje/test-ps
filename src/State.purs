module State(
  stateInit,
  stateNext,
  stateGet
) where
import Control.Monad.ST
import Prelude
import Timing
import World
import Data.Vector2
import Data.Vector4
import Data.Vector (toArray, Vec(Vec), vNegate, vlength, scale)

import Data.Array
import Math (sin, abs, log)

stateInit = do
  timing <- timingInit
  newSTRef (worldInit timing)

stateGet = readSTRef

stateNext ref = do
  state <- readSTRef ref
  timing <- timingNext state.timing
  writeSTRef ref (worldNext state timing)
