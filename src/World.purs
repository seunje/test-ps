module World(
  worldInit,
  worldNext,
  worldZero,
  colors,
  pos,
  color,
  positions,
  getSprings
) where
import Timing
import Prelude
import Data.Vector2
import Data.Array
import Data.Vector4
import Data.Maybe (maybe, fromJust)
import Partial.Unsafe (unsafePartial)
import Data.Vector

type Particle =
  { pos :: Vec2 Number
  , vel :: Vec2 Number
  , color :: Vec4 Number
  }

pos o = o.pos
color o = o.color

type World =
  { timing :: Timing
  , particles :: Array Particle
  , springs :: Array {a :: Int, b :: Int}
  }

colors = particlesToList color
positions = particlesToList pos

particleToList f o = toArray (f o)
particlesToList f = concatMap $ particleToList f

unsafeIndex v i = unsafePartial $ fromJust $ index v i

getSprings :: forall s a. (Particle -> Vec s Number) -> World -> Array Number
getSprings g world =
  let
    fm f = map (\l-> toArray $ g $ unsafeIndex world.particles (f l)) world.springs
    a = fm (\l-> l.a)
    b = fm (\l-> l.b)
  in
    concat $ zipWith (\x y-> concat [x, y]) a b

red = i4 + l4
green = j4 + l4
blue = k4 + l4

-- Hooke's law
-- F = -k * X
springF :: Number -> Vec2 Number -> Vec2 Number -> Vec2 Number
springF k p1 p2 = vec2 k k * (p2 + vNegate p1)

-- Coulomb's law
-- F = (q1 * q2) / (r^2))
staticF :: Number -> Number -> Vec2 Number -> Vec2 Number -> Vec2 Number
staticF q1 q2 p1 p2 = scale (q1 * q2) z
    where
    dsqr = distanceSquared p1 p2
    dir = normalize $ direction p1 p2
    z = scale (1.0 / dsqr) dir

particleNext :: World -> Particle -> Particle
particleNext world prev =
  let
    dt = scale (world.timing.dt * 0.001)
    k = vec2 0.1 0.1
    springs = foldr (\x z-> z + x.pos + (vNegate prev.pos)) (vec2 0.0 0.0) world.particles
    acc = k * springs
    vel = prev.vel + dt acc
    pos = prev.pos + dt vel
  in
    {
      pos: pos,
      vel: vel,
      color: prev.color
    }

mkParticle :: Number -> Number -> Vec4 Number -> Particle
mkParticle x y color = {
  pos: vec2 x y,
  vel: vec2 0.0 0.0,
  color: color
}

worldZero :: World 
worldZero = worldInit {t: 0.0, dt: 0.0}

worldInit :: Timing -> World
worldInit timing = {
    timing: timing,
    particles: [
      mkParticle 0.5 (-0.5) red,
      mkParticle (-0.1) 0.25 green,
      mkParticle 0.2 0.5 blue,
      mkParticle 0.0 0.0 red
    ],
    springs: [
      {a: 0, b: 1},
      {a: 1, b: 2},
      {a: 2, b: 3},
      {a: 3, b: 0}
    ]
  }

worldNext :: World -> Timing -> World
worldNext world timing = {
    timing: timing,
    particles: map (particleNext world) world.particles,
    springs: world.springs
  }
