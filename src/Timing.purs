module Timing(getTime, timingInit, timingNext, Timing) where

import Data.DateTime.Instant
import Data.Time.Duration
import Control.Monad.Eff
import Control.Monad
import Control.Monad.Eff.Now
import Prelude

type Timing =
  { t :: Number
  , dt :: Number}

unMilliseconds (Milliseconds x) = x

getTime = unMilliseconds <$> unInstant <$> now

timingInit :: forall a. Eff (now :: NOW | a) Timing
timingInit = getTime >>= \t -> pure { t: t, dt: 0.0 }

timingNext :: forall a. Timing -> Eff (now :: NOW | a) Timing
timingNext prev = getTime >>= \t -> pure { t: t, dt: t - prev.t }
